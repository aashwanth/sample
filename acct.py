from decimal import Decimal

class Transaction:

	transactions = []

	def MoveMoney(self, amount, source, dest):
		try:
			transaction = [amount, source, dest]
			self.transactions.append(transaction)
		except Exception as e:
			print str(e)

	def Close(self):
		for transaction in self.transactions:
			try:
				amount = transaction[0]
				source = transaction[1]
				dest = transaction[2]
				source.balance -= amount
				dest.balance += amount
			except Exception as e:
				print str(e)

class Account:

	def __init__(self):
		self.balance = Decimal("0.0")

	def Balance(self):
		return self.balance

class account:

	def NewAccount(self):
		account = Account()
		return account

	def NewTransaction(self):
		transaction = Transaction()
		return transaction

	
